package com.vamsi.cowsnbulls;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by vamsi on 1/23/2015.
 */
public class DictionaryFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dictionary_layout, container, false);
        ArrayAdapter<String> adapter = new DictionaryAdapter(getActivity());
        ((GridView) view.findViewById(R.id.dictionary_list)).setAdapter(adapter);
        return view;
    }

    public class DictionaryAdapter extends ArrayAdapter<String> {
        public DictionaryAdapter(Activity activity) {
            super(activity, R.layout.dictionary_list_item);
        }

        @Override
        public int getCount() {
            return Dictionary.getInstance().getCount();
        }

        @Override
        public String getItem(final int position) {
            return Dictionary.getInstance().getWord(position);
        }
    }
}
