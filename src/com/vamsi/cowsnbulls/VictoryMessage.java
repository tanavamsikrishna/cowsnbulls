package com.vamsi.cowsnbulls;

import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by vamsi on 1/27/2015.
 */
public class VictoryMessage implements DialogInterface.OnClickListener {
    protected void show(int noOfGuesses) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Main.activity);
        builder.setTitle("You Win!");
        builder.setMessage("You have guessed the word correctly in " + noOfGuesses + " guesses");
        builder.setPositiveButton("OK", this);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.cancel();
        Main.activity.openDrawer();
    }
}
