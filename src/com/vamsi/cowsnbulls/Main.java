package com.vamsi.cowsnbulls;

import android.app.Activity;
import android.app.FragmentManager;

/**
 * To be used to do stuff at the start of game
 * Created by vamsi on 1/20/2015.
 */
public class Main {

    protected static MainActivity activity;

    protected static void setMainActivity(MainActivity activity) {
        Main.activity = activity;
    }

    /**
     * To read the dictionary words into Dictionary class
     *
     * @param activity
     */
    protected static void prepareDictionary(Activity activity) {
        Dictionary.getInstance();
    }
}
