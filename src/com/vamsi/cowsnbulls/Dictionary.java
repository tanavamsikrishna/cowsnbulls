package com.vamsi.cowsnbulls;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by vamsi on 1/20/2015.
 */
public class Dictionary {

    private static Dictionary instance;

    private List<char[]> dictionaryWords;

    protected static Dictionary getInstance() {
        if (instance == null) {
            instance = new Dictionary();
        }
        return instance;
    }

    private Dictionary() {
        InputStream input = Main.activity.getResources().openRawResource(R.raw.dictionary);
        InputStreamReader reader = new InputStreamReader(input);
        int charsRead = 0;
        dictionaryWords = new ArrayList<char[]>();
        while (charsRead > -1) {
            try {
                char[] word = new char[4];
                charsRead = reader.read(word);
                reader.skip(2);
                if (charsRead > -1)
                    dictionaryWords.add(word);
            } catch (IOException e) {
                e.printStackTrace();
                dictionaryWords.clear();
                break;
            }
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected boolean containsWord(String inputWord) {
        boolean wordFound;
        for (char[] word : dictionaryWords) {
            wordFound = true;
            for (int i = 0; i < 4; i++) {
                if (word[i] != inputWord.charAt(i)) {
                    wordFound = false;
                    break;
                }
            }
            if (wordFound)
                return true;
        }
        return false;
    }

    protected String getWord(int pos) {
        String word = "";
        for (char c : dictionaryWords.get(pos)) {
            word += c;
        }
        return word;
    }

    protected int getCount() {
        return dictionaryWords.size();
    }

    protected char[] produceRandomWord() {
        Double randNum = new Random(System.currentTimeMillis()).nextDouble();
        int index = (int) (randNum * getCount());
        return dictionaryWords.get(index);
    }
}
