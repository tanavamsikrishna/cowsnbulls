package com.vamsi.cowsnbulls;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Created by vamsi on 1/28/2015.
 */
public class StatsFragment extends Fragment {
    ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stats_fragment, container, false);
        GridView gridView = (GridView) view.findViewById(R.id.prev_match_results);
        gridView.setAdapter(getPrevMatchAdapter());
        ((TextView) view.findViewById(R.id.total_matches_played)).setText(String.valueOf(Stats.getInstance().getMatchesCount()));
        float avgGuesses = Stats.getInstance().avgNoGuess();
        int avgGuessRoundedInt = Math.round(avgGuesses * 100);
        float avgGuessesRounded = ((float) avgGuessRoundedInt) / 100;
        ((TextView) view.findViewById(R.id.avg_guesses)).setText(String.valueOf(avgGuessesRounded));
        return view;
    }

    private ListAdapter getPrevMatchAdapter() {
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.stats_fragment_row) {
            @Override
            public int getCount() {
                int matchesCount = Stats.getInstance().getMatchesCount();
                if (matchesCount >= 5) {
                    return 10;
                } else {
                    return matchesCount * 2;
                }
            }

            @Override
            public String getItem(int position) {
                if (position % 2 == 0) {
                    return Stats.getInstance().getMatchStat(position / 2).word;
                } else {
                    return String.valueOf(Stats.getInstance().getMatchStat((position - 1) / 2).number);
                }
            }
        };
        return adapter;
    }
}
