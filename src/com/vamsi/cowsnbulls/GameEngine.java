package com.vamsi.cowsnbulls;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by vamsi on 1/20/2015.
 */
public class GameEngine {
    private static GameEngine instance;
    private static Set<Character> trimmableChars;

    private boolean gameInProgress = false;
    private char[] word;
    private Map<String, String> guessedWords;

    {
        trimmableChars = new HashSet<Character>();
        trimmableChars.add(' ');
        trimmableChars.add('\n');
    }

    protected static void takeInstance(GameEngine engine) {
        instance = engine;
    }

    protected static GameEngine getInstance() {
        if (instance == null) {
            instance = new GameEngine();
        }
        return instance;
    }

    public void startNewGame() {
        //LinkedHaspMap to preserve the order of insertions of Map entries
        guessedWords = new LinkedHashMap<String, String>();
        word = Dictionary.getInstance().produceRandomWord();
        gameInProgress = true;
    }

    protected void endGame() {
        int numberOfGuesses = guessedWords.size() + 1;
        guessedWords.clear();
        word = null;
        gameInProgress = false;
        Main.activity.endGame(numberOfGuesses);
    }

    protected void continueGame() {
        Main.activity.continueGame();
    }

    /**
     * If this function is getting called, it means that the word
     * guessed is a valid word
     *
     * @param guessedWord
     */
    protected void processWordGuess(String guessedWord) {
        int bulls = 0;
        int cows = 0;
        guessedWord = trimGuessWord(guessedWord);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == j) {
                    if (guessedWord.charAt(j) == word[i]) {
                        bulls++;
                    }
                } else {
                    if (guessedWord.charAt(j) == word[i]) {
                        cows++;
                    }
                }
            }
        }

        if (bulls == 4) {
            Stats.getInstance().gameWon(String.valueOf(word), guessedWords.size() + 1);
            endGame();
        } else {
            String resultString = "" + bulls;
            if (bulls == 1) {
                resultString += "Bull, ";
            } else {
                resultString += "Bulls, ";
            }
            resultString += cows;
            if (cows == 1) {
                resultString += "Cow";
            } else {
                resultString += "Cows";
            }
            guessedWords.put(guessedWord, resultString);
            continueGame();
        }
    }

    protected DualStr getPrevGuessedWord(int position) {
        if (position >= guessedWords.entrySet().size()) {
            return null;
        }
        int pos = 0;
        for (Map.Entry<String, String> entry : guessedWords.entrySet()) {
            if (pos == position) {
                DualStr dual = new DualStr();
                dual.str1 = entry.getKey();
                dual.str2 = entry.getValue();
                return dual;
            } else {
                pos++;
            }
        }
        return null;
    }

    private String trimGuessWord(String guess) {
        guess = guess.toLowerCase();
        while (trimmableChars.contains(guess.charAt(0))) {
            guess = guess.substring(1);
        }
        while (trimmableChars.contains(guess.charAt(guess.length() - 1))) {
            guess = guess.substring(0, guess.length() - 1);
        }
        return guess;
    }

    protected GuessValidity isValidGuess(String guess) {
        if (guess == null || guess.length() < 4) {
            return GuessValidity.INVALID_WORD;
        }

        guess = trimGuessWord(guess);

        if (guess.length() != 4) {
            return GuessValidity.INVALID_WORD;
        }

        Set<Character> chars = new HashSet<Character>();
        for (char c : guess.toCharArray()) {
            chars.add(c);
        }
        if (chars.size() != 4) {
            return GuessValidity.INVALID_WORD;
        }

        if (guessedWords.keySet().contains(guess)) {
            return GuessValidity.ALREADY_GUESSED;
        }

        if (!Dictionary.getInstance().containsWord(guess)) {
            return GuessValidity.WORD_NOT_IN_DICT;
        }

        return GuessValidity.VALID_GUESS;
    }

    protected boolean isGameInProgress() {
        return gameInProgress;
    }

    protected int getNumberOfWordsGuessed() {
        return guessedWords.size();
    }

    enum GuessValidity {
        WORD_NOT_IN_DICT,
        ALREADY_GUESSED,
        INVALID_WORD,
        VALID_GUESS
    }
}
