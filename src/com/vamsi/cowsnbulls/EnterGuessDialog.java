package com.vamsi.cowsnbulls;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Krishna on 09-Feb-15.
 */
public class EnterGuessDialog {
    private EditText editText;
    private AlertDialog alertDialog;
    private ClickListener listener;

    void show(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.enter_your_guess);
        editText = new EditText(activity);
        builder.setView(editText);
        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("Cancel", null);
        alertDialog = builder.create();

        listener = new ClickListener();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(listener);
            }
        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    listener.onClick(null);
                }
                return false;
            }
        });

        alertDialog.show();
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String text = editText.getText().toString();
            GameEngine.GuessValidity validity = GameEngine.getInstance().isValidGuess(text);
            if (validity == GameEngine.GuessValidity.VALID_GUESS) {
                alertDialog.cancel();
                GameEngine.getInstance().processWordGuess(text);
            } else if (validity == GameEngine.GuessValidity.WORD_NOT_IN_DICT ||
                    validity == GameEngine.GuessValidity.INVALID_WORD) {
                editText.setText("");
                alertDialog.setTitle(R.string.invalid_enter_your_guess);
            } else if (validity == GameEngine.GuessValidity.ALREADY_GUESSED) {
                editText.setText("");
                alertDialog.setTitle(R.string.invalid_enter_new_guess);
            }
        }
    }

}
