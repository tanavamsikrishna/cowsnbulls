package com.vamsi.cowsnbulls;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by vamsi on 1/28/2015.
 */
public class Stats {
    private static Stats instance;
    private static String GAME_STATS_FILE = "gameStats";

    protected static Stats getInstance() {
        return instance;
    }

    private List<KeyValue> prevGameStats;

    private Stats() {
        prevGameStats = new LinkedList<KeyValue>();
    }

    protected KeyValue getMatchStat(int pos) {
        return prevGameStats.get(pos);
    }

    protected int getMatchesCount() {
        return prevGameStats.size();
    }

    protected float avgNoGuess() {
        float sum = 0;
        for (KeyValue kv : prevGameStats) {
            sum += kv.number;
        }
        return sum / prevGameStats.size();
    }

    protected static void loadPrevStats() {
        File statsFile = new File(Main.activity.getFilesDir(), GAME_STATS_FILE);
        if (!statsFile.exists()) {
            instance = new Stats();
            return;
        }
        try {
            FileReader reader = new FileReader(statsFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String json = bufferedReader.readLine();
            Stats stats = new Gson().fromJson(json, Stats.class);
            Stats.instance = stats;
            reader.close();
            bufferedReader.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    protected static void storeStats() {
        File statsFile = new File(Main.activity.getFilesDir(), GAME_STATS_FILE);
        try {
            if (statsFile.exists()) {
                statsFile.delete();
                statsFile.createNewFile();
            }
            FileOutputStream stream = new FileOutputStream(statsFile);
            String json = new Gson().toJson(instance);
            stream.write(json.getBytes());
            stream.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    protected void gameWon(String word, int noOfGuesses) {
        KeyValue keyValue = new KeyValue();
        keyValue.number = noOfGuesses;
        keyValue.word = word;
        for (int i = 0; i < prevGameStats.size(); i++) {
            if (prevGameStats.get(i).number >= noOfGuesses) {
                prevGameStats.add(i, keyValue);
                return;
            }
        }
        prevGameStats.add(keyValue);
    }

    protected class KeyValue {
        public int number;
        public String word;
    }
}
