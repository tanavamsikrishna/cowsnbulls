package com.company;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        File inputFile = new File("input.txt");
        File outputFile = new File("output.txt");
        try {
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            FileReader inputReader = new FileReader(inputFile);
            BufferedReader inputBuffReader = new BufferedReader(inputReader);
            String line;
            Set<Character> chars = new HashSet<Character>();
            while ((line = inputBuffReader.readLine()) != null) {
                if (line.length() != 4) {
                    continue;
                }
                if (line.charAt(0) >= 'A' && line.charAt(0) <= 'Z') {
                    continue;
                }
                chars.clear();
                for (int i = 0; i < 4; i++) {
                    chars.add(line.charAt(i));
                }
                if (chars.size() != 4) {
                    continue;
                }

                outputStream.write(line.getBytes());
                outputStream.write('\n');

            }
            outputStream.close();
            inputReader.close();
            inputBuffReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // write your code here
    }
}
