package com.vamsi.cowsnbulls;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class MainActivity extends Activity {
    private static int DRAWER_DELAY = 200;
    private static String GAME_STATE_FILE = "gameState";

    protected FragmentManager fm;
    protected DrawerLayout drawerLayout;
    private int activeDrawerItemIndex = -1;
    private ArrayAdapter<String> navAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        fm = getFragmentManager();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navAdapter = new ArrayAdapter<String>(
                this,
                R.layout.drawer_list_item,
                NavDrawerHelper.getInstance().getNavigationDrawerItems());

        ListView drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerList.setAdapter(navAdapter);
        drawerList.setOnItemClickListener(new NavDrawerListener());

        Main.setMainActivity(this);
        Main.prepareDictionary(this);

        loadGameState();
        Stats.loadPrevStats();
    }

    /**
     * Created by vamsi on 1/23/2015.
     */
    public class NavDrawerListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectDrawerItem(position);
        }
    }

    public void selectDrawerItem(int position) {
        Fragment selectedFragment = null;
        String tag = null;
        drawerLayout.closeDrawers();
        if (position == activeDrawerItemIndex && position != 4 && position != 0) {
            return;
        }
        activeDrawerItemIndex = position;
        switch (position) {
            case 0:
                if (!GameEngine.getInstance().isGameInProgress()) {
                    GameEngine.getInstance().startNewGame();
                    navAdapter.notifyDataSetChanged();
                }
                tag = GameFragment.class.getName();
                Fragment gameFragment = getFragmentManager().findFragmentByTag(tag);
                if (gameFragment == null) {
                    selectedFragment = new GameFragment();
                } else {
                    selectedFragment = gameFragment;
                }
                break;
            case 1:
                tag = StatsFragment.class.getName();
                Fragment statsFragment = getFragmentManager().findFragmentByTag(tag);
                if (statsFragment == null) {
                    selectedFragment = new StatsFragment();
                } else {
                    selectedFragment = statsFragment;
                }
                break;
            case 2:
                tag = DictionaryFragment.class.getName();
                Fragment dicFragment = getFragmentManager().findFragmentByTag(tag);
                if (dicFragment == null) {
                    selectedFragment = new DictionaryFragment();
                } else {
                    selectedFragment = dicFragment;
                }
                break;
            case 3:
                tag = InstructionsFragment.class.getName();
                Fragment insFragment = getFragmentManager().findFragmentByTag(tag);
                if (insFragment == null) {
                    selectedFragment = new InstructionsFragment();
                } else {
                    selectedFragment = insFragment;
                }
                break;
            case 4:
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"talupula.krishna@gmail.com"});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback on CowsNBulls");
                emailIntent.setType("message/rfc822");
                startActivity(Intent.createChooser(emailIntent, "Choose email client"));
                return;
            default:
                break;
        }
        getFragmentManager().beginTransaction().replace(R.id.content_frame, selectedFragment, tag).commit();
    }

    protected void showEnterGuessFragment() {
        new EnterGuessDialog().show(this);
    }

    protected void endGame(int noOfGuesses) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        new VictoryMessage().show(noOfGuesses);
        StatsFragment fragment = (StatsFragment) getFragmentManager().findFragmentByTag(StatsFragment.class.getName());
        if (fragment != null) {
            fragment.adapter.notifyDataSetChanged();
        }
        navAdapter.notifyDataSetChanged();
    }

    protected void continueGame() {
        selectDrawerItem(0);
    }

    protected void openDrawer() {
        new Handler().postDelayed(openDrawableRunnable(), DRAWER_DELAY);
    }

    private Runnable openDrawableRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        writeGameState();
        Stats.storeStats();
    }

    private void writeGameState() {
        if (GameEngine.getInstance().isGameInProgress()) {
            Gson gson = new Gson();
            String str = gson.toJson(GameEngine.getInstance());
            File gameStateFile = new File(getFilesDir(), GAME_STATE_FILE);
            try {
                gameStateFile.createNewFile();
                FileOutputStream stream = new FileOutputStream(gameStateFile);
                stream.write(str.getBytes());
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadGameState() {
        try {
            File file = new File(getFilesDir(), GAME_STATE_FILE);
            if (file.exists()) {
                FileReader fileReader = new FileReader(file);
                BufferedReader reader = new BufferedReader(fileReader);
                String json = reader.readLine();
                GameEngine engine = new Gson().fromJson(json, GameEngine.class);
                GameEngine.takeInstance(engine);
                reader.close();
                fileReader.close();
                file.delete();
                selectDrawerItem(0);
            } else {
                openDrawer();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

//todo : add action bar and search for dictionary