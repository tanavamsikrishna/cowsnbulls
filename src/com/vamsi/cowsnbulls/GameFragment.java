package com.vamsi.cowsnbulls;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by vamsi on 1/26/2015.
 */
public class GameFragment extends Fragment {
    ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_fragment, container, false);
        adapter = new GameFragmentAdapter(getActivity());
        ((GridView) (view.findViewById(R.id.guessed_words_list))).setAdapter(adapter);
        Button enterGuessButton = (Button) view.findViewById(R.id.new_guess_button);
        enterGuessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).showEnterGuessFragment();
            }
        });
        return view;
    }

    private class GameFragmentAdapter extends ArrayAdapter<String> {

        public GameFragmentAdapter(Context context) {
            super(context, R.layout.game_fragment_row);
        }

        @Override
        public int getCount() {
            return GameEngine.getInstance().getNumberOfWordsGuessed() * 2;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            String text = getItem(position);
            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.game_fragment_row, null);
            }
            TextView word = (TextView) convertView.findViewById(R.id.game_fragment_text);
            word.setText(text);
            return convertView;
        }

        @Override
        public String getItem(int position) {
            DualStr dualStr = GameEngine.getInstance().getPrevGuessedWord((getCount() / 2) - (position / 2) - 1);
            if (position % 2 == 0) {
                return dualStr.str1;
            } else {
                return dualStr.str2;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }
}
