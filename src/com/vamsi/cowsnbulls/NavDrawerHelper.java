package com.vamsi.cowsnbulls;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vamsi on 1/23/2015.
 */
public class NavDrawerHelper {

    private static NavDrawerHelper instance;

    static {
        instance = new NavDrawerHelper();
    }

    protected static NavDrawerHelper getInstance() {
        return instance;
    }

    protected List<String> getNavigationDrawerItems() {
        List<String> listItems = new ArrayList<String>() {
            @Override
            public String get(int index) {
                if (index == 0 && GameEngine.getInstance().isGameInProgress()) {
                    return "Continue Game";
                }
                return super.get(index);
            }
        };
        listItems.add("New Game");
        listItems.add("Stats");
        listItems.add("View Dictionary");
        listItems.add("Instructions");
        listItems.add("Feedback");
        return listItems;
    }
}
